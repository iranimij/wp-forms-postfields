<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.iranimij.com
 * @since      1.0.0
 *
 * @package    Wp_Forms_Postfields
 * @subpackage Wp_Forms_Postfields/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Forms_Postfields
 * @subpackage Wp_Forms_Postfields/admin
 * @author     iman heydari <iranimij@gmail.com>
 */
class Wp_Forms_Postfields_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
    if ( ! function_exists('is_plugin_inactive')) {
      require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
    }

    if (is_plugin_inactive('wpforms-master/wpforms.php')) {
      add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ]);
    }
    if (!did_action("wpforms_loaded")){

    }
		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}
  public function admin_notice_missing_main_plugin(){
    if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

    $message = sprintf(
    /* translators: 1: Plugin name 2: Elementor */
      esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'wp-forms-postfields' ),
      '<strong>' . esc_html__( 'wp forms postfields', 'wp-forms-postfields' ) . '</strong>',
      '<strong>' . esc_html__( 'WP-Forms', 'wp-forms-postfields' ) . '</strong>'
    );

    printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );
  }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Forms_Postfields_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Forms_Postfields_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-forms-postfields-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Forms_Postfields_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Forms_Postfields_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */


		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-forms-postfields-admin.js', array( 'jquery' ), $this->version, false );


	}

  public function add_post_fields_to_wp_forms($args) {

    $new_args = $args + [
      "post_title" => "post title",
      "post_content" => "post content",
      "post_author" => "post author",
      "post_date" => "post date",
      "post_id" => "post id",
      ];

    return $new_args;
  }

  public function filter_added_fields_to_wp_forms($content, $tag) {
    switch ($tag) {

      case 'post_title':
        $content = str_replace('{' . $tag . '}', get_the_title(), $content);
        break;

      case 'post_content':
        $content = str_replace('{' . $tag . '}', get_the_content(), $content);
        break;

      case 'post_author':
        $content = str_replace('{' . $tag . '}', get_the_author(), $content);
        break;

      case 'post_date':
        $content = str_replace('{' . $tag . '}', get_the_date(), $content);
        break;

      case 'post_id':
        $content = str_replace('{' . $tag . '}', get_the_ID(), $content);
        break;
    }
    return $content;
  }

}
