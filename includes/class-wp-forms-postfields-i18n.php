<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.iranimij.com
 * @since      1.0.0
 *
 * @package    Wp_Forms_Postfields
 * @subpackage Wp_Forms_Postfields/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_Forms_Postfields
 * @subpackage Wp_Forms_Postfields/includes
 * @author     iman heydari <iranimij@gmail.com>
 */
class Wp_Forms_Postfields_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wp-forms-postfields',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
