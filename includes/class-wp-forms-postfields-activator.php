<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.iranimij.com
 * @since      1.0.0
 *
 * @package    Wp_Forms_Postfields
 * @subpackage Wp_Forms_Postfields/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Forms_Postfields
 * @subpackage Wp_Forms_Postfields/includes
 * @author     iman heydari <iranimij@gmail.com>
 */
class Wp_Forms_Postfields_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
