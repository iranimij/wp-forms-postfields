<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.iranimij.com
 * @since      1.0.0
 *
 * @package    Wp_Forms_Postfields
 * @subpackage Wp_Forms_Postfields/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Forms_Postfields
 * @subpackage Wp_Forms_Postfields/includes
 * @author     iman heydari <iranimij@gmail.com>
 */
class Wp_Forms_Postfields_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
